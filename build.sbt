name := "Conandron"

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.10.3"

resolvers ++= Seq(
  "Twitter Repository" at "http://maven.twttr.com/"
)

libraryDependencies ++= Seq(
    "com.typesafe.slick" %% "slick"                 % "2.0.2",
    "org.slf4j"          % "slf4j-nop"              % "1.6.4",
    "mysql"              % "mysql-connector-java"   % "5.1.31",
    "com.twitter"        % "finagle-core_2.10"      % "6.12.1",
    "com.twitter"        % "finagle-http_2.10"      % "6.12.1",
    "org.json4s"         %% "json4s-jackson"        % "3.2.9",
    "com.google.guava"   % "guava"                  % "16.0.1",
    "org.scalikejdbc"    %% "scalikejdbc"           % "2.1.0",
    "ch.qos.logback"     %  "logback-classic"       % "1.1.2",
    "com.zaxxer"         % "HikariCP"               % "1.4.0"
  )