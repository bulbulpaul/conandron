package jp.bbp.connector

import javax.sql.DataSource

import scalikejdbc._
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}

object DBConnector {

  GlobalSettings.loggingSQLAndTime = LoggingSQLAndTimeSettings(
    //    enabled = true,
    enabled = false,
    logLevel = 'DEBUG,
    warningEnabled = true,
    warningThresholdMillis = 1000L,
    warningLogLevel = 'WARN
  )

  val dataSource:DataSource = {
    val config = new HikariConfig()
    config.setMaximumPoolSize(300)
    config.setMinimumIdle(300)
    config.setDataSourceClassName("com.mysql.jdbc.Driver")
    config.addDataSourceProperty("url", "jdbc:mysql://bulbulpaul.com:3306/Conandron")
    config.addDataSourceProperty("user", "conandron")
    config.addDataSourceProperty("password", "conandron")
    new HikariDataSource(config)
  }

  ConnectionPool.add('conandron, new DataSourceConnectionPool(dataSource))
}

class DBConnector {
  def initTable() = {
    val user = """Create table Users (
      |user_id  int(11)
      |name VARCHAR(254)
      |mail VARCHAR(254)
      |passwd VARCHAR(254)
      |exp  int(11)
      |location VARCHAR(64)
      |login_date TIMESTAMP
      |)""".stripMargin
    val division =
      """Create Table Division (
        |user_id  int(11)
        |name VARCHAR(255)
        |)""".stripMargin
    val badge =
      """Create Table Badge (
        |id int(11)
        |name VARCHAR(255)
        |)""".stripMargin
    val userBadge =
      """Create Table UserBadge (
        |user_id  int(11)
        |badge_id int(11)
        |)""".stripMargin
    val rank =
      """Create Table Rank (
        |id int(11)
        |name VARCHAR(64)
        |)""".stripMargin
    val sqls:List[String] = List(user, division, badge, userBadge, rank)

    DB autoCommit(
      implicit session =>
        for (sql <- sqls){
          SQL(sql).execute().apply()
        }
      )
  }

  def select(table:String, columns:List[String], cond:String) = {

    val sql = s"""Select $columns Form $table Where $cond"""
    DB readOnly(
      implicit session =>
          SQL(sql).execute().apply()
      )
  }
}
