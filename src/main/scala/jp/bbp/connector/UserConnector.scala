package jp.bbp.connector

import java.sql.Timestamp

import scala.slick.driver.MySQLDriver.simple._

class UserConnector {

  def main (args: Array[String]) {
    val users = TableQuery[Users]
    val division = TableQuery[Division]
    val badge = TableQuery[Badge]

    Database.forURL("jdbc:mysql://bulbulpaul.com:3306/Conandron", driver="com.mysql.jdbc.Driver", user = "conandron", password = "conandron" ) withSession{
      implicit session =>
        // Create Table
        (users.ddl ++ division.ddl ++ badge.ddl).create

        // TestData
        users ++= Seq(
          (1, "admin", "admin@test.com", "admin", 10L, "Hyogo/Japan", new Timestamp(1405550751)),
          (2, "user", "user@test.com", "user", 14130948L, "Osaka/Japan", new Timestamp(1405570731)),
          (3, "guest", "guest@test.com", "guest", 101234L, "Tokyo/Japan", new Timestamp(1405520751)))

        division ++= Seq(
          (1, "atdd"),
          (2, "web"))

        badge ++= Seq(
          (1, "Java"),
          (2, "Python"))

    }
  }

  def getUser(id:String) = {
    Database.forURL("jdbc:mysql://bulbulpaul.com:3306/Conandron", driver="com.mysql.jdbc.Driver", user = "conandron", password = "conandron" ) withSession{
      implicit session =>
        val users = TableQuery[Users]
        users.list
    }
  }

  def auth(pass:String) = {
    Database.forURL("jdbc:mysql://bulbulpaul.com:3306/Conandron", driver="com.mysql.jdbc.Driver", user = "conandron", password = "conandron" ) withSession{
      implicit session =>
        val users = TableQuery[Users]
        users.map(_.name)
    }
  }
}

/**
 * ユーザー情報
 */
class Users(tag: Tag) extends Table[(Int, String, String, String, Long, String, Timestamp)](tag, "Users") {
  // id, name, mail, pass, loginDate
  def id = column[Int]("USER_ID", O.PrimaryKey)
  def name = column[String]("USER_NAME", O.NotNull)
  def mail = column[String]("MAIL", O.NotNull)
  def password = column[String]("PASS", O.NotNull)
  def exp = column[Long]("EXP", O.NotNull)
  def location = column[String]("LOCATION")
  def loginDate = column[Timestamp]("LOGIN_DATE")
  def * = (id, name, mail, password, exp, location, loginDate)
}

/**
 * 所属情報
 */
class Division(tag:Tag) extends Table[(Int, String)](tag, "Division"){
  def id = column[Int]("DIVISION_ID", O.PrimaryKey)
  def name = column[String]("DIVISION_NAME", O.NotNull)
  def * = (id ,name)
}

/**
 * バッジ情報
 */
class Badge(tag: Tag) extends Table[(Int, String)](tag, "Badge"){
  def id = column[Int]("BADGE_ID", O.PrimaryKey)
  def name = column[String]("BADGE_NAME", O.NotNull)
  def * = (id ,name)
}

/**
 * ユーザーとバッジの関係情報
 * @param tag
 */
class UserBadge(tag:Tag) extends Table[(Int, Int)](tag, "UserBadge"){
  def userId = column[Int]("USER_ID", O.NotNull)
  def badgeId = column[Int]("BADGE_ID", O.PrimaryKey )
  def * = (userId, badgeId)
}

/**
 * ランク情報
 * @param tag
 */
class Rank(tag:Tag) extends Table[(Long, Long, String)](tag, "Rank"){
  def startExp = column[Long]("START_EXP", O.NotNull)
  def endExp = column[Long]("END_EXP", O.NotNull)
  def rankName = column[String]("RANK_NAME", O.NotNull)
  def * = (startExp, endExp, rankName)
}