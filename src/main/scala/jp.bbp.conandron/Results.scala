package jp.bbp.Conandron

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._


object ResultBuilder{
  def buildResult(status:String, result:Result):JObject = {
    ("status" -> status) ~ ("result" -> result.toJson())
  }
}


trait Result {
  def toJson():JObject
}

/**
 * Use success response
 */
case class UserResult (name:String, department:String, location:String, badge:List[String], channel:List[String], exp:Long, rank:Int ) extends Result {
  @Override
  def toJson():JObject = {
    ("name" -> name) ~
      ("rank" -> rank) ~
      ("exp" -> exp) ~
      ("badge" -> badge) ~
      ("location" -> location) ~
      ("department" -> department) ~
      ("channel" -> channel)
  }
}

/**
 * Use error response
 * @param message error description
 */
case class ErrorResult (message:String) extends Result {
  @Override
  def toJson():JObject = {
    ("message" -> message)
  }
}
