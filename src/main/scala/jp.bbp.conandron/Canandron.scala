package jp.bbp.Conandron

import java.net.InetSocketAddress
import java.util.UUID
import java.util.concurrent.{Executors, TimeUnit}
import com.google.common.base.Splitter
import com.twitter.finagle.http.Http
import com.twitter.finagle.builder.{Server, ServerBuilder}
import com.twitter.finagle.service.TimeoutFilter
import com.twitter.finagle.{Service, SimpleFilter, GlobalRequestTimeoutException}
import com.twitter.util.{Future, FuturePool, FutureTransformer, Duration, Timer}
import jp.bbp.connector.{UserConnector, Users}
import org.jboss.netty.handler.codec.http.{HttpResponseStatus, DefaultHttpResponse, HttpRequest, HttpResponse}
import org.jboss.netty.handler.codec.http.HttpResponseStatus.{NOT_FOUND, INTERNAL_SERVER_ERROR}


object Canandron {
  def main(args:Array[String]){

    val user = new UserConnector()

//    user.main(new Array[String](0))

    val service =
      new ExceptionFilter andThen
      new HeaderFilter andThen
//      new HttpTimeoutFilter(Duration(1, TimeUnit.SECONDS)) andThen
      new RestService()

    print("サーバーを起動します。")
    val server: Server = ServerBuilder()
      .codec(Http())
      .bindTo(new InetSocketAddress(4040))
      .name("httpserver")
      .build(service)
    print("サーバーを起動しました。")
  }

  class ExceptionFilter extends SimpleFilter[HttpRequest, HttpResponse]{

    val transformer = new FutureTransformer[HttpResponse, HttpResponse]{
      override def map(value:HttpResponse):HttpResponse =
        value
      override def handle(throwable: Throwable):HttpResponse =
        Responses.InternalServerError(throwable.getMessage)
    }
    // Apply method is where the filtering takes place.
    def apply(request: HttpRequest, service: Service[HttpRequest, HttpResponse]) = {
      // Use transformedBy method to deal with both the normal and error responses.
      service(request).transformedBy(transformer)
    }
  }

  // An trial filter that adds a header to the request and another
  // to the response.
  class HeaderFilter extends SimpleFilter[HttpRequest, HttpResponse] {

    def apply(request: HttpRequest, service: Service[HttpRequest, HttpResponse]) = {
      // Look for a X-Request-ID header and add one if missing.
      val id = request.getHeader("X-Request-ID")
      if (id == null) {
        request.setHeader("X-Request-ID",
          UUID.randomUUID().toString)
      }

      // Add X-Processes to the response.
      service(request).onSuccess(r => {
        r.addHeader("X-Processed", "TRUE")
      })
    }
  }

  // Invoke the correct underlying service based on the incoming url.
  class RestService extends Service[HttpRequest, HttpResponse] {

    val okService = new SimpleService()

    val apiService = new APIService()

    // timeout for the server.
    val timeoutService = new SimpleService(Some(1200))

    val futurePool = FuturePool(Executors.newFixedThreadPool(4))
    // Used to split URIs.
    val splitter = Splitter.on('/').omitEmptyStrings()

    def apply(req: HttpRequest) = {

      val path = splitter.split(req.getUri).iterator()
//      val path =  req.getUri.split("/")
      println("url = " + req.getUri + ", param = " + req.getContent)

      // Match the pieces of the path
      if (path.hasNext) {
        path.next() match {
          // Path starts with /ok so use ok service.
          case "api" => futurePool {
            Responses.OK(req,
              (req: HttpRequest) => { apiService("getUser") })}
//          case ("api", version:Int, method:String) => futurePool {
//            // See Responses.OK comments below.
//            Responses.OK(req,
//              (req: HttpRequest) => { okService("versions") })}
//          case ("api", method:String) => futurePool {
//            Responses.OK(req,
//              (req: HttpRequest) => { apiService(method, new Map()) })}
//          case "timeout" => futurePool {
//            Responses.OK(req,
//              (req: HttpRequest) => { timeoutService(req.getUri) })}
          case _ => Future.value(Responses.NotFound())
        }
      } else {
        // No match so not found.
        Future.value(Responses.NotFound())
      }
    }
  }

  // A very simple service that returns a model object.
  class SimpleService(val waitFor: Option[Int] = None) {

    def apply(name: String) = {
      // is used to timeout the service.
      waitFor.foreach(t => this.synchronized { wait(t) })
      // Return the model object.
      new String("helloworld : " + name)
//      new Users(UUID.randomUUID().toString, name, "Some Street")
    }
  }

  class APIService(val waitFor: Option[Int] = None) {

    val connector = new UserConnector
    def apply(method:String) = {
      method match {
        case "getUser" => connector.getUser("test")
        case _ => "No User"
      }
    }
  }
}
