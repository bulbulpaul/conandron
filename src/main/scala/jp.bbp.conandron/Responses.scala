package jp.bbp.Conandron

import jp.bbp.Conandron.Result
import org.jboss.netty.handler.codec.http.{HttpRequest, DefaultHttpResponse, HttpResponse, HttpResponseStatus}
import org.jboss.netty.buffer.ChannelBuffer
import org.jboss.netty.handler.codec.http.HttpVersion.HTTP_1_1
import org.jboss.netty.handler.codec.http.HttpResponseStatus.{NOT_FOUND, INTERNAL_SERVER_ERROR}
import org.jboss.netty.util.CharsetUtil.UTF_8
import org.jboss.netty.buffer.ChannelBuffers.copiedBuffer
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._



object Responses {

  // Create an HttpResponse from a status and some content.
  private def respond(status: HttpResponseStatus, content: ChannelBuffer): HttpResponse = {
    val response = new DefaultHttpResponse(HTTP_1_1, status)
    response.setHeader("Content-Type", "application/json")
    response.setHeader("Cache-Control", "no-cache")
    response.setContent(content)
    response
  }

  private def createResponse(status:String, response:Result) = {
    (status, response) match {
      case ("success", x:UserResult) => ("status" -> status) ~ ("results" -> x)
      case ("error", x:String) => ("status" -> status) ~ ("message" -> x)
    }
  }

  object OK {
    def apply(req: Result, service: (HttpRequest) => Object): HttpResponse = {
      val resJson = createResponse("success", req)
      respond(HttpResponseStatus.OK,
        copiedBuffer(compact(render(resJson)), UTF_8))
    }
  }

  object NotFound {
    def apply(): HttpResponse  = {
      val resJson = createResponse("error", new ErrorResult("Not Found"))
      respond(NOT_FOUND,
        copiedBuffer(compact(render(resJson)), UTF_8))
    }
  }

  object InternalServerError {
    def apply(message: String): HttpResponse = {
      val resJson = createResponse("error", new ErrorResult(message))
      respond(INTERNAL_SERVER_ERROR,
        copiedBuffer(compact(render(resJson)), UTF_8))
    }
  }

}
